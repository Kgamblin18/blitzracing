﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameMan : MonoBehaviour {

	public float lapDuration = 45f;
	public int player1Lap = 0;
	public int player2Lap = 0;
	public int player1Bullets = 0;
	public int player2Bullets = 0;
	public int player1Shield = 0;
	public int player2Shield = 0;
	public int targetscore = 3;
	public Text p1TimeText;
	public Text p2TimeText;

	private float roundEndTime;
	private float player1TimeLeft;
	private float player2TimeLeft;
	private bool roundEnded;

	
	public float lapCoolDown = 1f;
	
	
	private float player1LapReady;
	private float player2LapReady;
	public static GameMan gm = null;


	// Use this for initialization
	void Awake ()
	{
		if (gm == null)
		gm = this;
		else if (gm != this)
		Destroy (gameObject);
	}



	void Start ()
		{
			StartRound ();
		}


	void StartRound ()
	{
	roundEnded = false;
	player1TimeLeft = lapDuration;
	player2TimeLeft = lapDuration;
	
	}

	public void SetAmmo(int playerNumber, int amt)
	{
		if (playerNumber == 1) {
			player1Bullets--;
		} else if (playerNumber == 2) 
		{
			player2Bullets--;
		}
	}



	public void CountLaps(int playerNumber)
	{
		if (playerNumber == 1) 
		{
			if (Time.time > player1LapReady)
			{
				player1Lap++;
				player1TimeLeft = lapDuration;
				Debug.Log ("Player 1 laps: " + player1Lap);
				player1LapReady = Time.time + lapCoolDown;

			}
		} 
		else if (playerNumber == 2) 
		{
			if (Time.time > player2LapReady)
			{
			player2Lap++;
			player2TimeLeft = lapDuration;
			Debug.Log ("Player 2 got pickup");
			player2LapReady = Time.time + lapCoolDown;
			}
		}

		CheckIfGameOver();
	}

	

	// Update is called once per frame
	void Update () 
	{
		player1TimeLeft -= Time.deltaTime;
		p1TimeText.text = "Time Remaining: " + player1TimeLeft;

		player2TimeLeft -= Time.deltaTime;
		p2TimeText.text = "Time Remaining: " + player2TimeLeft;

		if (player1TimeLeft < 0 || player2TimeLeft < 0) 
		{
			EndRound ();
	
		}
	}


	public void CheckIfGameOver () 
	{
		if (player1Lap >= targetscore) {
			Debug.Log ("Player 1 wins");
		} else if (player2Lap >= targetscore) {
			Debug.Log ("Player 2 wins");

			Application.LoadLevel ("WinScene");
			Application.LoadLevel (3);
		} else if (player1Lap > player2Lap) {
			Debug.Log ("Player 1 wins");
		} else if (player2Lap > player1Lap) 
		{
			Debug.Log ("Player 2 wins");
		}

	

	}


void EndRound () 
	{
		roundEnded = true;
		Time.timeScale = 0;
		Debug.Log ("Round Ended");
	}
}