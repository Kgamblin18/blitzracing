﻿
using UnityEngine;
using System.Collections;

public class ProjectileLauncher : MonoBehaviour {
	
	public Rigidbody bullet;
	public float bulletForce = 700f;
	public float fireRate = .5f;
	public Transform bulletSpawn;
	public int playerNumber;

	public string playershoot = "Fire1";

	private float nextFireTime;
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown (playershoot) && Time.time > nextFireTime)
		{
			if (playerNumber == 1)
			{
				if (GameMan.gm.player1Bullets > 0)
				{
					LaunchProjectile();
				}

			}
			else if (playerNumber == 2)
			{
				if (GameMan.gm.player2Bullets > 0)
				{
					LaunchProjectile();
				}
				
			}


		}
	}
	
	void LaunchProjectile()
	{
		nextFireTime = Time.time + fireRate;
		Rigidbody clone = Instantiate (bullet, bulletSpawn.position, transform.rotation) as Rigidbody;
		clone.AddForce (bulletSpawn.forward * bulletForce);
		GameMan.gm.SetAmmo (playerNumber, 0);
	}
}