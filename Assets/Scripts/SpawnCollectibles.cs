﻿
using UnityEngine;
using System.Collections;

public class SpawnCollectibles : MonoBehaviour {
	
	public GameObject[] collectibles;
	public Transform[] spawnPoints;
	public int collectiblesToSpawn = 2;
	
	
	// Use this for initialization
	void Start () 
	{
		Spawn ();
	}
	
	void Spawn()
	{
		for (int i = 0; i < collectiblesToSpawn; i++) 
		{
			int randomPrefabIndex = Random.Range (0, collectibles.Length);
			int randomSpawnPointIndex = Random.Range (0, spawnPoints.Length);
			if (spawnPoints[randomSpawnPointIndex] != null)
			{
				GameObject clone = Instantiate (collectibles [randomPrefabIndex], spawnPoints [randomSpawnPointIndex].position, Quaternion.identity) as GameObject;
				spawnPoints[randomSpawnPointIndex]  = null;
			}
			else
			{
				i--;
			}
			
		}
		
	}
	
}