﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	
	public int startingHp = 3;
	
	public void TakeDamage()
	{
		startingHp --;
		Debug.Log ("hp: " + startingHp);
	}
	
}

