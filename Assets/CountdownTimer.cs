﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {
	
	public int count = 3;
	public Text countDownText;
	
	private bool complete;
	
	// Use this for initialization
	void Start () 
	{
		
		InvokeRepeating ("CountTick", 1, 1);
		
	}
	
	void CountTick () 
	{
		
		if (!complete) {
			count--;
			
			if (count == 1) {
				complete = true;
			}
			
			countDownText.text = count.ToString ();
		} else 
		{
			CountDownComplete ();
		}
		
		
	}
	
	// Update is called once per frame
	void CountDownComplete () 
	{
		countDownText.text = "Go!";
		Invoke ("ClearText", 1);
		
	}
	
	void ClearText()
	{
		countDownText.enabled = false;
		CancelInvoke ();
		//GameMan.gm.StartRound ();
	}
	
}