﻿using UnityEngine;
using System.Collections;

public class ShieldController : MonoBehaviour {

	public GameObject shield;
	public float duration = 10f;

	private float shieldOffTime;
	private bool shieldOn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.time > shieldOffTime && shieldOn) 
		{
			shieldOn = false;
			shield.SetActive (false);
		}
	}

	public void TurnOnShield()
	{
		shieldOn = true;
		shieldOffTime = Time.time + duration;
		shield.SetActive (true);
	}
}
