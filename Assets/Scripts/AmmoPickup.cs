﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		HoverMotor playerController = other.GetComponentInParent<HoverMotor> ();
		Debug.Log ("other: " + other);
		if (playerController != null) 
		{
			GameMan.gm.SetAmmo (playerController.playerNumber, 1);
			gameObject.SetActive(false);
		}
	}

}
