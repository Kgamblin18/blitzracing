﻿using UnityEngine;
using System.Collections;

public class ShieldPickup : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		HoverMotor playerController = other.GetComponentInParent<HoverMotor> ();
		if (playerController != null) 
		{
			//GameMan.gm.SetShield(playerController.playerNumber, 1);
			ShieldController shieldControllerScript = other.GetComponent<ShieldController>();
			shieldControllerScript.TurnOnShield();
			gameObject.SetActive(false);
		}
	}

}