﻿
using UnityEngine;
using System.Collections;

public class HoverMotor : MonoBehaviour {
	
	public float speed = 90f;
	public float turnSpeed = 5f;
	public float hoverForce = 65f;
	public float hoverHeight = 3.5f;
	public int playerNumber = 1;

	public string hAxis = "Horizontal";
	public string vAxis = "Vertical";
	public float slowSpeedHit;
	public float slowSpeedOffTrack = 45f;
	private float originalSpeed;
	private float speedUpTime;

	private float powerInput;
	private float turnInput;
	private Rigidbody carRigidbody;

	
	
	void Awake () 
	{
		carRigidbody = GetComponent <Rigidbody>();
		originalSpeed = speed;

	}
	

	 void Update () 
		{
		turnInput = Input.GetAxis (hAxis);
		powerInput = Input.GetAxis (vAxis);

		if (Time.time > speedUpTime) 
		{
			speed = originalSpeed;
		}

		}
	

	public void HitSlow(float duration)
	{
		speedUpTime = Time.time + duration;
		speed = slowSpeedHit;
	}

	void FixedUpdate()
	{
		Ray ray = new Ray (transform.position, -transform.up);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, hoverHeight))
		{
			//Debug.Log ("hit collider: " + hit.collider);

			if (hit.collider.GetComponent<TrackGround>() != null)
			{
				speed = originalSpeed;
			}
			else 
			{
				speed = slowSpeedOffTrack;
			}

			float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight;
			Vector3 appliedHoverForce = Vector3.up * proportionalHeight * hoverForce;
			carRigidbody.AddForce(appliedHoverForce, ForceMode.Acceleration);
		}
		
		carRigidbody.AddRelativeForce(0f, 0f, -powerInput * speed);
		carRigidbody.AddRelativeTorque(0f, turnInput * turnSpeed, 0f);
		
	}
}