﻿using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {


	void OnTriggerEnter(Collider other)
	{
		Bullet bulletScript = other.GetComponent<Bullet> ();
		if (bulletScript != null) 
		{
			other.gameObject.SetActive(false);
		}
	}
}
