﻿
using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {


	public float slowDuration = 3f;


	
	void OnCollisionEnter(Collision other)
	{
		HoverMotor motor = other.gameObject.GetComponent<HoverMotor> ();
		if (motor != null)
		{
			motor.HitSlow(slowDuration);
		}
		
		gameObject.SetActive (false);
	}
}